import { signInWithEmailAndPassword } from 'firebase/auth';
import type { RegisterMutate } from '../../../types';

const useAuthQuery = (options = {}) =>
  useMutation({
    mutationFn: (payload: RegisterMutate) =>
      signInWithEmailAndPassword(payload.auth, payload.email, payload.password),
    ...options,
  });

export default useAuthQuery;
