import { fileURLToPath, URL } from 'node:url';

import vue from '@vitejs/plugin-vue';
import AutoImport from 'unplugin-auto-import/vite';
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers';
import Components from 'unplugin-vue-components/vite';
import { defineConfig, loadEnv } from 'vite';

import pluginSvgVue from '@vuetter/vite-plugin-vue-svg';

export default defineConfig(({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  const plugins = [
    vue({ script: { defineModel: true, propsDestructure: true } }),
    pluginSvgVue(),
    AutoImport({
      dts: 'src/shared/types/auto-imports.d.ts',
      imports: [
        'vue',
        'pinia',
        'vitest',
        'vue-router',
        {
          'naive-ui': [
            'useDialog',
            'useMessage',
            'useNotification',
            'useLoadingBar',
          ],
          '@tanstack/vue-query': [
            'useQuery',
            'useQueries',
            'useMutation',
            'useIsFetching',
            'useQueryClient',
            'VueQueryPlugin',
          ],
        },
      ],
      eslintrc: {
        enabled: true,
      },
    }),
    Components({
      dirs: [],
      dts: 'src/shared/types/components.d.ts',
      resolvers: [NaiveUiResolver()],
    }),
  ];

  return {
    plugins,
    assetsInclude: ['**/*.mp4'],
    server: {
      port: 5173,
    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
        'helpers': fileURLToPath(new URL('./src/helpers', import.meta.url)),
      },
    },
  };
});
