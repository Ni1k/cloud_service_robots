export const ITEMS_CARDS = [
  {
    title: 'Управление роботами',
    details:
      'Отслеживайте, контролируйте и настраивайте своих роботов, где бы вы ни находились.',
    icon: '💻',
  },
  {
    title: 'Мониторинг и управление',
    details:
      'Получайте реальное время мониторинга и полный контроль над вашими роботами.',
    icon: '⚡️',
  },
  {
    title: 'Аналитика и отчетность',
    details: 'Генерируйте отчеты и анализируйте производительность роботов.',
    icon: '📈',
  },
  {
    title: 'Оптимизированная сборка',
    details:
      'Предварительно настроенная сборка с поддержкой многостраничного режима',
    icon: '📦',
  },
  {
    title: 'Интеграция и автоматизация',
    details:
      'Интегрируйте роботов в ваши системы и автоматизируйте рутинные задачи.',
    icon: '🛠️',
  },
  {
    title: 'Безопасность и надежность',
    details:
      'Обеспечьте безопасность и надежность вашей робототехники с нашим облачным сервисом.',
    icon: '🔑',
  },
];
