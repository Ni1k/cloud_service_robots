import { initializeApp } from 'firebase/app';

// ... other firebase imports

export const firebaseApp = initializeApp({
  apiKey: 'AIzaSyCAzalVHbitG1fkudtYZGIh5AqJ9MrVHj0',
  authDomain: 'robots-b331d.firebaseapp.com',
  projectId: 'robots-b331d',
  storageBucket: 'robots-b331d.appspot.com',
  messagingSenderId: '334672136901',
  appId: '1:334672136901:web:0427c90567b678c9557eac',
  measurementId: 'G-6H8HZY0JS4',
});
