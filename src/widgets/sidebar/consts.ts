import { NIcon } from 'naive-ui';
import folder from '@/assets/svg/folder.vue';
import machine from '@/assets/svg/machine.vue';

function renderIcon(icon: any) {
  return () => h(NIcon, { size: 28 }, { default: () => h(icon) });
}

export const options = [
  {
    label: 'Datasets',
    key: 'datasets',
    icon: renderIcon(folder),
    routeName: '/',
    accentColor: '#18a058',
  },

  {
    label: 'Machines',
    key: 'machines',
    icon: renderIcon(machine),
    routeName: '/machines',
    accentColor: '#d75c5c',
  },
];
