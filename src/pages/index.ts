import type { RouteRecordRaw } from 'vue-router';

import { datasetsPages } from './datasets';

import { loginPage } from './login';

import { machinesPages } from './machine';

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    children: [datasetsPages, loginPage, machinesPages],
  },

  {
    path: '/:catchAll(.*)*',
    name: 'error',
    component: () => import('./ErrorNotFound.vue'),
  },
];
