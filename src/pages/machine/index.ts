import type { RouteRecordRaw } from 'vue-router';

import { machinesPage } from './ui';

import { robotPage } from './robot';
export const machinesPages = {
  path: 'machines',
  name: 'machines',
  component: () => import('./MachinePages.vue'),
  children: [machinesPage, robotPage],
} as RouteRecordRaw;
