import { type GlobalThemeOverrides } from 'naive-ui';

export const darkThemeOverrides: GlobalThemeOverrides = {
  common: {
    bodyColor: 'rgb(24, 24, 28)',
  },

  Menu: {
    itemTextColor: '#858D96',
    itemIconColor: '#BFC7D0',

    itemIconColorCollapsed: '#858D96',

    groupTextColor: 'rgba(243, 246, 248, 1)',

    itemColorActive: 'rgba(236, 42, 42, 0.1)',
    itemIconColorActive: '#EC2A2A',
    itemTextColorActive: '#EC2A2A',

    itemTextColorHover: '#EC2A2A',
    itemIconColorHover: '#BC0808',

    colorInverted: '#ffffff',

    itemColorActiveInverted: '#BC0808',

    itemIconColorActiveCollapsedInverted: '#BC0808',

    itemTextColorInverted: '#858D96',
    itemIconColorInverted: '#BFC7D0',

    itemTextColorHoverInverted: '#EC2A2A',
    itemIconColorHoverInverted: '#BC0808',
  },
};

export const ThemeOverrides: GlobalThemeOverrides = {
  Menu: {
    itemTextColor: '#858D96',
    itemIconColor: '#BFC7D0',

    itemIconColorCollapsed: '#858D96',

    groupTextColor: 'rgba(243, 246, 248, 1)',

    itemColorActive: 'rgba(236, 42, 42, 0.1)',
    itemIconColorActive: '#EC2A2A',
    itemTextColorActive: '#EC2A2A',

    itemTextColorHover: '#EC2A2A',
    itemIconColorHover: '#BC0808',

    colorInverted: '#ffffff',

    itemColorActiveInverted: '#BC0808',

    itemIconColorActiveCollapsedInverted: '#BC0808',

    itemTextColorInverted: '#858D96',
    itemIconColorInverted: '#BFC7D0',

    itemTextColorHoverInverted: '#EC2A2A',
    itemIconColorHoverInverted: '#BC0808',
  },
};
