export * from './item-card';

import type { RouteRecordRaw } from 'vue-router';

export const machinesPage = {
  path: '',
  name: 'machines',
  component: () => import('./MachinePage.vue'),
} as RouteRecordRaw;
