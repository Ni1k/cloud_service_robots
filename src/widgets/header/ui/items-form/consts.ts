import type { FormItemRule, FormRules } from 'naive-ui';

export const emailRegexp =
  /^[\wA-zА-яЁё.+-]+@[A-zА-яЁё\d-]+\.[A-zА-яЁё\d\-.]+$/;

export const passRegex =
  /^(?=.*[0-9])(?=.*[.@_\-%$/\\*!])(?=.*[A-z])(?=.*[A-Z])[a-zA-Z0-9.@_\-%$/\\*!]{8,24}$/;

const base = {
  required: true,
  trigger: ['blur', 'change'],
};

const FIELD_REQUIRED = 'Поле обязательно для заполнения';

export const rulesInitial = (): FormRules => ({
  email: {
    ...base,
    validator: (rule: FormItemRule, value: string) => {
      if (!value) {
        return new Error(FIELD_REQUIRED);
      }

      if (value.length > 1000) {
        return new Error(
          'Доступен ввод числовых и текстовых значений длиной до 1000 символов',
        );
      }

      if (!emailRegexp.test(value)) {
        return new Error(
          'Адрес электронной почты нужно указывать в формате __@__._',
        );
      }

      return true;
    },
  },

  password: {
    ...base,
    validator: (rule, value: string) => {
      if (!value) {
        return new Error('Поле обязательно для заполнения');
      }

      return true;
    },
  },
});
