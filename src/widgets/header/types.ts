import { type Auth } from 'firebase/auth';

export interface RegisterMutate {
  auth: Auth;
  email: string;
  password: string;
}

export interface FormLogin {
  email: string | null;
  password: string | null;
}
