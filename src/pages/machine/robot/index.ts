import type { RouteRecordRaw } from 'vue-router';

export const robotPage = {
  path: '/machines/:id',
  name: 'robot',
  component: () => import('./RobotPage.vue'),
} as RouteRecordRaw;
