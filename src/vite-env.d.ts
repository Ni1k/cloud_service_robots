import type { EnvVars } from '@/shared/config';

declare module '*.vue' {
  import type { DefineComponent } from 'vue';

  const component: DefineComponent;
  export default component;
}

export interface ImportMetaEnv extends EnvVars {}

export interface ImportMeta {
  readonly env: ImportMetaEnv;
}
